#include <stdio.h>

#define SIZE 20

struct Matrice{
    double tab[SIZE][SIZE];
};

// Initialise les entrées avec la valeur 0.0
void initialiser(struct Matrice *matrice);
// Afficher sous la forme [   m11   m12   ...  m120 ]
//                        [   m21   m22   ...  m220 ]
//                        [   ...   ...   ...   ... ]
//                        [  m201  m202   ... m2020 ]
void afficher(const struct Matrice *matrice);
void additionner(const struct Matrice *m1,
        const struct Matrice *m2,
        struct Matrice *resultat);
void multiplier(const struct Matrice *m1,
        const struct Matrice *m2,
        struct Matrice *resultat);

int main(void){
    struct Matrice m;
    initialiser(&m);
    afficher(&m);
    return 0;
}

void initialiser(struct Matrice *matrice){
    int i, j;
    for(i = 0; i < SIZE; i++){
        for(j = 0; j < SIZE; j++){
            matrice->tab[i][j] = 0.0;
        }
    }
}

void afficher(const struct Matrice *matrice){
    int i, j;
    for(i = 0; i < SIZE; i++){
        printf("[ ");
        for(j = 0; j < SIZE; j++){
            printf("%4.2f ", matrice->tab[i][j]);
        }
        printf("]\n");
    }
}

void additionner(const struct Matrice *m1,
        const struct Matrice *m2,
        struct Matrice *resultat){
    int i, j;
    for(i = 0; i < SIZE; i++){
        for(j = 0; j < SIZE; j++){
            resultat->tab[i][j] = m1->tab[i][j] + m2->tab[i][j];
        }
    }
}

void multiplier(const struct Matrice *m1,
        const struct Matrice *m2,
        struct Matrice *resultat){
    int i, j, k, somme;
    for(i = 0; i < SIZE; i++){
        for(j = 0; j < SIZE; j++){
            somme = 0;
            for(k = 0; k < SIZE; k++){
                somme += m1->tab[i][k] * m2->tab[k][j];
            }
            resultat->tab[i][j] = somme;
        }
    }
}
