#include <stdio.h>
#include <stdlib.h>
// Types
// -----

struct QueueNode {
    char content;           // Contenu du noeud
    struct QueueNode *prev; // Noeud precedent
    struct QueueNode *next; // Noeud suivant
};

typedef struct {
    struct QueueNode *first; // Pointeur vers le premier noeud
    struct QueueNode *last;  // Pointeur vers le dernier noeud
} Queue;

// Prototypes
// ----------

Queue queueCreate();
int queueIsEmpty(const Queue *s);
void queuePush(Queue *s, char content);
char queuePop(Queue *s);
void queueDelete(Queue *s);

int main(void){
    Queue file = queueCreate();

    queuePush(&file, 'a');
    queuePush(&file, 'b');
    queuePush(&file, 'c');
    queuePush(&file, 'd');
    queuePush(&file, 'e');
    queuePush(&file, 'f');
    queuePush(&file, 'g');

    printf("%c\n", queuePop(&file));
    printf("%c\n", queuePop(&file));
    printf("%c\n", queuePop(&file));
    printf("%c\n", queuePop(&file));

    queueDelete(&file);
    return 0;
}

Queue queueCreate(){
    Queue res;
    res.first = NULL;
    res.last = NULL;
    return res;
}

int queueIsEmpty(const Queue *s){
    if(s->first){
        return 0;
    } else {
        return 1;
    }
}

void queuePush(Queue *s, char content){
    struct QueueNode *node = (struct QueueNode*)malloc(sizeof(struct QueueNode));
    if(node){
        node->content = content;
        node->next = NULL;
        node->prev = s->last;
        if(s->last){
            s->last->next = node;
        } else {
            s->first = node;
        }
        s->last = node;
    } else {
        fprintf(stderr, "Erreur d'allocation de memoire\n");
        exit(1);
    }
}

char queuePop(Queue *s){
    struct QueueNode *node = s->first;
    if(node){
        char res = node->content;
        if(node->next){
            node->next->prev = NULL;
        }
        s->first = node->next;
        free(node);
        return res;
    } else {
        fprintf(stderr, "Erreur fille vide\n");
        exit(1);
    }
}

void queueDelete(Queue *s){
    struct QueueNode *node;
    while(s->first){
        node = s->first;
        s->first = node->next;
        free(node);
    }
    s->last = NULL;
}
