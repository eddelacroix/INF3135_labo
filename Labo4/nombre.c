#include <stdio.h>

enum e_nombre {
    INT, FLOAT, DOUBLE
};

union u_nombre {
    int i;
    float f;
    double d;
};

struct nombre {
    enum e_nombre typeNombre; // Le type de la valeur
    union u_nombre valeur;    // La valeur
};

struct nombre max(struct nombre n1, struct nombre n2);

int main(void){
    struct nombre n1;
    n1.typeNombre = INT;
    n1.valeur.i = 10;
    struct nombre n2;
    n2.typeNombre = FLOAT;
    n2.valeur.f = 1.0f;
    struct nombre res = max(n1,n2);
    printf("%d\n", res.valeur.i);
    return 0;
}

struct nombre max(struct nombre n1, struct nombre n2){
    double v1, v2;

    switch(n1.typeNombre){
        case INT:
            v1 = (double)n1.valeur.i;
            break;
        case FLOAT:
            v1 = (double)n1.valeur.f;
            break;
        case DOUBLE:
            v1 = n1.valeur.d;
            break;
    }

    switch(n2.typeNombre){
        case INT:
            v2 = (double)n2.valeur.i;
            break;
        case FLOAT:
            v2 = (double)n2.valeur.f;
            break;
        case DOUBLE:
            v2 = n2.valeur.d;
            break;
    }

    if(v1 >= v2){
        return n1;
    } else {
        return n2;
    }
}
