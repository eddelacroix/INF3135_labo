#include <stdio.h>

int somme(int tab[], int size);

int main(int argc, char *argv[]) {
    if(argc != 2){
        printf("Erreur: Un seul argument permis\n");
        return 1;
    }
    
    printf("Hello, %s!\n", argv[1]);

    int tab[3] = {1,2,3};
    printf("somme = %d\n", somme(tab, 3));
    return 0;
}

int somme(int tab[], int size){
    int i;
    int res = 0;
    for(i = 0; i < size ; i++){
        res += tab[i];
    }
    return res;
}
