#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 200

struct Ville{
    char nom[16];
    char pays[5];
    long pop;
};

void afficherVille(struct Ville *ville, int rang);
int initVille(struct Ville *ville, char* str);

int main(int argc,char *argv[]){
    FILE* fichier = NULL;
    struct Ville ville;
    int rang = 1;
    char buffer[MAX_LINE + 1];

    if(argc != 2){
        fprintf(stderr, "usage: nom du fichier d'entre\n");
        exit(1);
    }

    fichier = fopen(argv[1], "r");
    if(!fichier){
        fprintf(stderr, "Erreur d'ouverture du fichier\n");
        exit(1);
    }

    printf("Rang Nom             Pays Population\n");
    printf("---- --------------- ---- ----------\n");
    fgets(buffer, MAX_LINE, fichier);
    while(!feof(fichier)){
        if(!initVille(&ville, buffer)){
            fprintf(stderr, "Erreur de format\n");
        }
        afficherVille(&ville, rang);
        rang++;
        fgets(buffer, MAX_LINE, fichier);
    }
    return 0;
}

void afficherVille(struct Ville *ville, int rang){
    printf("%04d %-15s %-4s %10d\n", rang, ville->nom, ville->pays, ville->pop);
}

int initVille(struct Ville *ville, char* str){
    char* virgule;
    int nchar;
    char* pstr = str;

    virgule = strchr(pstr, ',');
    if(!virgule){
        return 1;
    }
    nchar = virgule - pstr;
    if(nchar > 15){
        nchar = 15;
    }
    strncpy(ville->nom, pstr, nchar);
    ville->nom[nchar] = '\0';

    pstr = virgule + 1;
    virgule = strchr(pstr, ',');
    if(!virgule){
        return 1;
    }
    nchar = virgule - pstr;
    if(nchar > 4){
        nchar = 4;
    }
    strncpy(ville->pays, pstr, nchar);
    ville->pays[nchar] = '\0';

    pstr = virgule + 1;
    ville->pop = strtol(pstr, &pstr, 10);
    if((*pstr) != '\0'){
        return 1;
    }
    return 0;
}
